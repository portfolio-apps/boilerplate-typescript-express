import parser from 'solidity-parser-antlr';

export function solidityParser(
    input: string
): parser.ASTNode {
    try {
        const ast: parser.ASTNode = parser.parse(input, {});
        return ast;
    } catch (e) {
        console.log(e)
    }
}