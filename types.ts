export type Response = {
    imports: Array<string>,
    contracts: Array<string>
}