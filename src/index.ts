import express from "express";
import bodyParser from "body-parser";

import { solidityParser } from "../services/Parser";
import { Response } from '../types';

const app = express();
const port = 3001; // default port to listen

app.use(bodyParser.json())

// define a route handler for the default home page
app.get( "/", ( req, res ) => {
    res.send("<h1>Solidity Parser</h1>");
});

app.post( "/analyze", ( req, res ) => {
    const response: Response = {
        imports: [],
        contracts: []
    };
    const input = req.body.input;
    const parsed = solidityParser(input);
    parsed['children'].forEach((child: any) => {
        if (child.hasOwnProperty('path')) {
            response.imports.push(child.path);
        }
        if (child.hasOwnProperty('name')) {
            response.contracts.push(child.name);
        }
    })
    res.json(response);
});

app.post( "/parsed", ( req, res ) => {
    const input = req.body.input;
    const parsed = solidityParser(input);
    res.json(parsed);
});

// start the Express server
app.listen( port, () => {
    console.log( `server started at http://localhost:${ port }` );
});